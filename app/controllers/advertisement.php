<?php

class Advertisement extends Controller
{
    public function index(): void
    {
        $database = $this->model('Database');
        $data = $database->selectAdvertisements();
        $this->view('advertisement/index', $data);
    }
}
