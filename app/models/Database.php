<?php

class Database
{
    private function connect(): mysqli
    {
        $serverName = "localhost";
        $userName = "root";
        $password = "";
        $database = "rabit";

        $connection = new mysqli($serverName, $userName, $password, $database);

        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }

        return $connection;
    }

    public function selectData(string $tableName): array
    {
        $connection = $this->connect();
        $sql = "SELECT * FROM $tableName";
        $result = $connection->query($sql);
        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        } else {
            echo "No rows found";
        }
        $connection->close();
        return $data;
    }

    public function selectAdvertisements(): array
    {
        $connection = $this->connect();
        $data = [];

        $sql = "SELECT advertisements.id, advertisements.title, users.name FROM advertisements LEFT JOIN users ON advertisements.user_id = users.id";

        $result = $connection->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        } else {
            echo "No rows found";
        }

        $connection->close();
        return $data;
    }
}
