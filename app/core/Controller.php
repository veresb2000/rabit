<?php

class Controller
{

    public function model($model): object
    {
        require_once '../app/models/' . $model . '.php';
        return new $model();
    }

    public function view($view, array $data = []): void
    {
        require_once '../app/views/' . $view . '.php';
    }
}
